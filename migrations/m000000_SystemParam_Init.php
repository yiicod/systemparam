<?php

class m000000_SystemParam_Init extends CDbMigration
{
    public function up()
    {
        $this->execute('
                CREATE TABLE `SystemParam` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `paramKey` varchar(100) NOT NULL,
                  `paramValue` varchar(100) NOT NULL,
                  `description` varchar(255) DEFAULT NULL,
                  `validator` varchar(50) DEFAULT NULL,
                  `isDefault` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
    }

    public function down()
    {
        $this->execute('
            DROP TABLE `SystemParam`;
        ');
    }
}
