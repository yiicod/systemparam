<?php

/**
 * This is the model class for table "SystemConfig".
 *
 * The followings are the available columns in table 'SystemConfig':
 *
 * @property int $id
 * @property string $description
 * @property string $paramKey
 * @property string $validator
 * @property string $paramValue
 * @property string $isDefault
 */

namespace yiicod\systemparam\models;

use CActiveDataProvider;
use CActiveRecord;
use CDbCriteria;
use CMap;
use CSort;
use Yii;

class SystemParamModel extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'SystemParam';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['paramKey, validator, paramValue', 'required'],
            ['paramKey', 'unique', 'on' => 'insert'],
            ['isDefault', 'safe'],
            ['paramKey', 'length', 'max' => 100],
            ['description', 'length', 'max' => 255],
            ['paramValue', 'yiicod.systemparam.models.validators.YiiConditionalValidator',
                'if' => [
                    ['validator', 'compare', 'compareValue' => 'email'],
                ],
                'then' => [
                    ['paramValue', 'email'],
                ],
            ],
            ['paramValue', 'yiicod.systemparam.models.validators.YiiConditionalValidator',
                'if' => [
                    ['validator', 'compare', 'compareValue' => 'numerical'],
                ],
                'then' => [
                    ['paramValue', 'numerical'],
                ],
            ],
            ['paramValue', 'yiicod.systemparam.models.validators.YiiConditionalValidator',
                'if' => [
                    ['validator', 'compare', 'compareValue' => 'url'],
                ],
                'then' => [
                    ['paramValue', 'url'],
                ],
            ],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, paramKey, validator, paramValue, description', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('systemparam', 'ID'),
            'description' => Yii::t('systemparam', 'Description'),
            'paramKey' => Yii::t('systemparam', 'Name'),
            'validator' => Yii::t('systemparam', 'Validator'),
            'paramValue' => Yii::t('systemparam', 'Value'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     *                             based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('paramKey', $this->paramKey, true);
        $criteria->compare('validator', $this->validator);
        $criteria->compare('paramValue', $this->paramValue, true);
        $sort = [];
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldIsDefault'], $this->attributeNames())) {
            $sort = [
                'defaultOrder' => [
                    Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldIsDefault'] => CSort::SORT_ASC,
                    'id' => CSort::SORT_ASC,
                ],
            ];
        }

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => [
                'pageSize' => Yii::app()->getComponent('systemparam')->pageSize,
            ],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return SystemConfigModel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        $behaviors['SystemParamBehavior'] = [
            'class' => 'yiicod\systemparam\models\behaviors\SystemParamBehavior',
        ];

        if (file_exists(Yii::getPathOfAlias('application.models.behaviors.XssBehavior').'.php')) {
            $behaviors['XssBehavior'] = [
                'class' => 'application.models.behaviors.XssBehavior',
            ];
        }

        return CMap::mergeArray(parent::behaviors(), $behaviors);
    }
}
