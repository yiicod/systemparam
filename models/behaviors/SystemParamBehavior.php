<?php

namespace yiicod\systemparam\models\behaviors;

use Yii;
use CActiveRecordBehavior;

class SystemParamBehavior extends CActiveRecordBehavior
{
    /**
     * Set key.
     *
     * @param string $value Param key
     */
    public function setParamKey($value)
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamKey'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamKey']} = $value;
        }
    }

    /**
     * Set value.
     *
     * @param string $value Param value
     */
    public function setParamValue($value)
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamValue'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamValue']} = $value;
        }
    }

    /**
     * Set validator.
     *
     * @param string $value Param value
     */
    public function setValidator($value)
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldValidator'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldValidator']} = $value;
        }
    }

    /**
     * Set description.
     *
     * @param string $value Param description
     */
    public function setDescription($value)
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldDescription'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldDescription']} = $value;
        }
    }

    /**
     * Set description.
     *
     * @param string $value Param description
     */
    public function setIsDefault($value)
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldIsDefault'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldIsDefault']} = $value;
        }
    }

    /**
     * Get key.
     *
     * @return string $value Param key
     */
    public function getParamKey()
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamKey'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamKey']};
        }

        return;
    }

    /**
     * Get value.
     *
     * @return string $value Param value
     */
    public function getParamValue()
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamValue'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamValue']};
        }

        return;
    }

    /**
     * Get validator.
     *
     * @return string $value Param value
     */
    public function getValidator()
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldValidator'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldValidator']};
        }

        return 'string';
    }

    /**
     * Get description.
     *
     * @return string $value Param description
     */
    public function getDescription()
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldDescription'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldDescription']};
        }

        return '';
    }

    /**
     * Get description.
     *
     * @param string $value Param description
     */
    public function getIsDefault()
    {
        if (in_array(Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldIsDefault'], $this->getOwner()->attributeNames())) {
            return intval($this->getOwner()->{Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldIsDefault']});
        }

        return intval(false);
    }
}
