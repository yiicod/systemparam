<?php
$this->breadcrumbs = [
    Yii::t('systemparam', 'Manage Params'),
];
?>

<h1><?php echo Yii::t('systemparam', 'Manage Params') ?></h1>

<?php
$this->widget('zii.widgets.grid.CGridView', [
    'id' => 'systemparam-model-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'items table table-striped table-bordered',
    'filter' => $model,
    'columns' => [
        Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamKey'],
        Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldDescription'],
        [
            'name' => Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['fieldParamValue'],
            'class' => 'vendor.vitalets.x-editable-yii.EditableColumn',
            'editable' => [
                'placement' => 'left',
                'apply' => '!$data->getIsDefault()',
                'type' => 'text',
                'url' => $this->createUrl('update'),
            ],
        ],
    ],
]);
?>
