<?php

return [
    'commandMap' => [
        'params' => [
            'class' => 'yiicod\systemparam\commands\SystemParamCommand',
        ],
    ],
    'modelMap' => [
        'SystemParam' => [
            'alias' => 'yiicod\systemparam\models\SystemParamModel',
            'class' => 'yiicod\systemparam\models\SystemParamModel',
            'fieldParamKey' => 'paramKey',
            'fieldParamValue' => 'paramValue',
            'fieldValidator' => 'validator',
            'fieldDescription' => 'description',
            'fieldIsDefault' => 'isDefault',
        ],
    ],
    'autoUpdate' => false,
    'cacheDuration' => 28800,
    'controllers' => [
        'controllerMap' => [
            'admin' => [
                'systemParam' => 'yiicod\systemparam\controllers\admin\ParamController',
            ],
        ],
        'admin' => [
            'systemParam' => [
                'layout' => '',
                'filters' => [],
                'accessRules' => [],
            ],
        ],
    ],
    'components' => [
        'bootstrap' => [
            'class' => 'vendor.clevertech.yii-booster.src.components.Bootstrap',
        ],
        'cache' => [
            'class' => 'CFileCache',
        ],
    ],
];
