<?php

namespace yiicod\systemparam\controllers\admin;

use Controller;
use Yii;
use CMap;

class ParamController extends Controller
{
    public $defaultAction = 'admin';

    public function init()
    {
        parent::init();

        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        if (Yii::app()->getComponent('systemparam')->controllers[$module][Yii::app()->controller->id]['layout']) {
            $this->layout = Yii::app()->getComponent('systemparam')->controllers[$module][Yii::app()->controller->id]['layout'];
        }
    }

    public function filters()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;

        return CMap::mergeArray(parent::filters(), CMap::mergeArray(
                                [
                            'accessControl',
                            'ajaxOnly + update',
                                ], Yii::app()->getComponent('systemparam')->controllers[$module][Yii::app()->controller->id]['filters']
                        )
        );
    }

    public function accessRules()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;

        return CMap::mergeArray(parent::accessRules(), Yii::app()->getComponent('systemparam')->controllers[$module][Yii::app()->controller->id]['accessRules']
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return [
            'admin' => [
                'class' => 'yiicod\systemparam\actions\admin\systemParam\AdminAction',
            ],
            'update' => [
                'class' => 'yiicod\systemparam\actions\admin\systemParam\UpdateAction',
            ],
        ];
    }
}
