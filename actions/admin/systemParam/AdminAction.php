<?php

namespace yiicod\systemparam\actions\admin\systemParam;

use yiicod\systemparam\actions\BaseAction;
use CHtml;
use Yii;

/**
 * Creates a new model.
 *
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * If creation is successful, the browser will be redirected to the 'admin' page.
 */
class AdminAction extends BaseAction
{
    public $view = 'yiicod.systemparam.views.admin.systemParam.admin';

    public function run()
    {
        $modelCms = Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['class'];
        $controller = $this->getController();
        $model = new $modelCms();

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET[CHtml::modelName($model)])) {
            $model->attributes = $_GET[CHtml::modelName($model)];
        }

        $controller->render($this->view, [
            'model' => $model,
        ]);
    }
}
