<?php

namespace yiicod\systemparam\actions\admin\systemParam;

use CHttpException;
use CJSON;
use Yii;
use yiicod\systemparam\actions\BaseAction;
use yiicod\systemparam\SystemParam;

class UpdateAction extends BaseAction
{
    /**
     * Ajax update from grid view.
     *
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     */
    public function run()
    {
        $model = $this->loadModel(Yii::app()->request->getParam('pk', 0), Yii::app()->getComponent('systemparam')->modelMap['SystemParam']['class']);
        if ($model->getIsDefault()) {
            throw new CHttpException(409, Yii::t('systemparam', 'You cannot update this field. Field is default.'));
        }
        $model->{Yii::app()->request->getParam('name')} = Yii::app()->request->getParam('value');
        if (!$model->save()) {
            throw new CHttpException(409, $model->getError(Yii::app()->request->getParam('name')));
        }
        Yii::app()->cache->delete(SystemParam::CACHE_KEY);
        echo CJSON::encode([]);
        Yii::app()->end();
    }
}
