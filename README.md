SystemParam
===========

With this extension you easy config your Yii::app()->params from admin panel. 
You need install extension with composer and run command:
```php
php yiic params sync
```
This is command for synchronize your php config with db.

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist yiicod/systemparam "*"
```

or add

```json
"yiicod/systemparam": "*"
```

Link to source: https://bitbucket.org/yiicod/systemparam

Config ( This is all config for extensions ):
---------------------------------------------

```php
aliases' => array(
	'vendor' => 'application.vendor',
	'editable' => 'vendor.vitalets.x-editable-yii',
),
'import' => array(
        'editable.*',
),
'components' => array(
    ...
    'editable' => [
	'class' => 'editable.EditableConfig',
	    'form' => 'jqueryui',
	    'mode' => 'inline',
	    'defaults' => [
		'emptytext' => 'Click to edit',
		'ajaxOptions' => ['dataType' => 'json'], //usefull for json exchange with server
	],
    ],
    'systemparam' => array(
        'class' => 'yiicod\systemparam\SystemParam',
        'modelMap' => array(
            'SystemParam' => array(
                'alias' => 'yiicod\systemparam\models\SystemParamModel',
                'class' => 'yiicod\systemparam\models\SystemParamModel',
                'fieldParamKey' => 'paramKey',
                'fieldParamValue' => 'paramValue',
                'fieldValidator' => 'validator',
                'fieldDescription' => 'description',
                'fieldIsDefault' => 'isDefault',
            )
        ),
        'autoUpdate' => YII_DEBUG,
        'debug' => YII_DEBUG,
        'controllers' => array(
            'controllerMap' => array(
                'admin' => array(
                    'systemParam' => 'yiicod\systemparam\controllers\admin\ParamController',
                )
            ),
            'admin' => array(
                'systemParam' => array(
                    'layout' => '/layouts/column1',
                    'filters' => array('accessControl'),
                    'accessRules' => array(
                        array('allow',
                            'actions' => array('update', 'admin'),
                            'roles' => array('admin'),
                        ),
                        array('deny', // deny all users
                            'users' => array('*'),
                        ),
                    ),
                )
            ),
        ),
        'components' => array(),
    ),
    ...
)

'preload' => array('systemparam')
```
Should work with diffenrent urlManager config, but didn't test with different url rules.
