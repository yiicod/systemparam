<?php

namespace yiicod\systemparam\commands;

use CConsoleCommand;
use Yii;

class SystemParamCommand extends CConsoleCommand
{
    /**
     * Run send mail.
     */
    public function actionSync()
    {
        Yii::app()->systemparam->mergeParams(true);
        echo "Sync params done \n";
    }
}
