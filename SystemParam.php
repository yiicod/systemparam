<?php

namespace yiicod\systemparam;

use CApplicationComponent;
use CConsoleApplication;
use CJSON;
use CLogger;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Yii;
use yiicod\systemparam\helpers\Map;

/**
 * Cms extension settings.
 *
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */
class SystemParam extends CApplicationComponent
{
    const ARRAY_KEYS_SEPARATOR = ' | ';
    const PARAM_IS_NOT_SET = 'CONFIG_VALUE_IS_NOT_SET';
    const CACHE_KEY = 'YII_SYSTEM_PARAM';

    /**
     * @var bool
     */
    private $_initialized = false;

    /**
     * @var type
     */
    private $_originalParams = [];

    /**
     * @var bool
     */
    public $commandMap = [];

    /**
     * @var array Controllers settings
     */
    public $controllers = [];

    /**
     * @var array table settings
     */
    public $modelMap = [];

    /**
     * Set auto update config.
     */
    public $autoUpdate = null;

    /**
     * @var array components
     */
    public $components = [];

    /**
     * Cache durations.
     *
     * @var int
     */
    public $cacheDuration = 0;

    /**
     * @var type
     */
    public $pageSize = 30;

    public function mergeParams()
    {
        $paramModel = $this->modelMap['SystemParam']['class'];

        $paramsArray = $this->_originalParams->toArray(); //Yii::app()->getParams()->toArray();
        //auto update config
        $arrayOfKeys = $this->getArrayKeysRecursivelyInString($paramsArray);
        $models = $paramModel::model()->findAll();
        $records = [];
        foreach ($models as $model) {
            if (self::PARAM_IS_NOT_SET !== $this->getParamValueByKey($model->getParamKey(), $paramsArray)) {
                $records[$model->getParamKey()] = clone $model;
            } else {
                $model->delete();
            }
        }
        foreach ($arrayOfKeys as $pk) {
            $model = new $paramModel();
            if (isset($records[$pk])) {
                $model->setIsNewRecord(false);
                $model->setScenario('update');
                $model->setAttributes($records[$pk]->getAttributes(), false);
            }

            $model->setParamKey($pk);

            $value = $this->getParamValueByKey($pk, $paramsArray);

            if (null !== @CJSON::decode($value) && is_array(@CJSON::decode($value))) {
                $param = CJSON::decode($value);
                $model->setParamValue(is_bool($param['value']) ? intval($param['value']) : $param['value']);
                $model->setDescription(isset($param['description']) ? $param['description'] : '');
                $model->setIsDefault(isset($param['isDefault']) ? $param['isDefault'] : intval(false));
                $model->setValidator(isset($param['validator']) ? $param['validator'] : 'string');
            } else {
                $model->setParamValue(is_bool($value) ? intval($value) : $value);
                $model->setValidator('string');
                $model->setDescription('');
                $model->setIsDefault(intval(false));
            }

            if ($model->isNewRecord || (count(array_diff([
                    $model->getParamValue(),
                    $model->getValidator(),
                    $model->getDescription(),
                    $model->getIsDefault(), ], [
                    $records[$pk]->getParamValue(),
                    $records[$pk]->getValidator(),
                    $records[$pk]->getDescription(),
                    $records[$pk]->getIsDefault(),
                        ]
                    )
                ) && $this->autoUpdate)
            ) {
                if (!$model->save()) {
                    Yii::log($pk.'  '.CJSON::encode($model->getErrors()), CLogger::LEVEL_ERROR, 'system.systemparam');
                }
            }
        }

        $this->flushCache();

        return true;
    }

    protected function flushCache()
    {
        if (isset(Yii::app()->cache)) {
            Yii::app()->cache->delete(self::CACHE_KEY);
        }
    }

    protected function prepareParams()
    {
        $this->_initialized = true;
        Yii::app()->setComponent('systemparam', $this, false);

        $paramModel = $this->modelMap['SystemParam']['class'];

        //If table not exists
        if (Yii::app() instanceof CConsoleApplication && null === Yii::app()->db->schema->getTable($paramModel::model()->tableName())) {
            Yii::log('Table '.$paramModel::model()->tableName().' not exists');

            return false;
        }
        $loadParamsFromCache = $this->loadParamsFromCache();

        //Load from db params and update cache
        if (0 === $this->cacheDuration || false === $loadParamsFromCache) {
            $this->loadParamsFromDb();
            //Set params to cahce
            if (isset(Yii::app()->cache)) {
                Yii::app()->cache->set(self::CACHE_KEY, CJSON::encode(Yii::app()->params), $this->cacheDuration);
            }
        }

        return true;
    }

    /**
     * Load params from db.
     *
     * @return bolean
     */
    protected function loadParamsFromCache()
    {
        if ((isset(Yii::app()->cache) && $this->cacheDuration > 0 && null !== @CJSON::decode(Yii::app()->cache->get(self::CACHE_KEY)))) {
            Yii::app()->params = CJSON::decode(Yii::app()->cache->get(self::CACHE_KEY));

            return true;
        }

        return false;
    }

    /**
     * Load params from db.
     *
     * @return bolean
     */
    protected function loadParamsFromDb()
    {
        $paramModel = $this->modelMap['SystemParam']['class'];

        $records = Yii::app()->db->createCommand()
            ->select('*')
            ->from($paramModel::model()->tableName())
            ->queryAll();

        foreach ($records as $record) {
            $result = $this->generateArrayForParamValue($record[$this->modelMap['SystemParam']['fieldParamKey']], $record[$this->modelMap['SystemParam']['fieldParamValue']]);
            Yii::app()->params = Map::mergeArray(Yii::app()->params, $result);
        }

        return true;
    }

    /**
     * Generate array of keys recursively in string.
     *
     * @param $dataArray
     *
     * @return array
     *
     * @author Chaykovskiy Roman
     */
    protected function getArrayKeysRecursivelyInString($dataArray)
    {
        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($dataArray));
        $keys = [];
        foreach ($iterator as $key => $value) {
            // Build long key name based on parent keys
            for ($i = $iterator->getDepth() - 1; $i >= 0; --$i) {
                $key = $iterator->getSubIterator($i)->key().self::ARRAY_KEYS_SEPARATOR.$key;
            }
            $keys[] = $key;
        }

        return $keys;
    }

    /**
     * Return param value or false if param not exist.
     *
     * @param $pk
     * @param $array
     *
     * @return bool
     *
     * @author Chaykovskiy Roman
     */
    protected function getParamValueByKey($pk, $array)
    {
        $keys = explode(self::ARRAY_KEYS_SEPARATOR, $pk);

        foreach ($keys as $key) {
            $array = isset($array[$key]) ? $array[$key] : self::PARAM_IS_NOT_SET;
            if ($array == self::PARAM_IS_NOT_SET) {
                break;
            }
        }

        return $array;
    }

    /**
     * Generate multidimensional array for pk with value.
     *
     * @param $pk can be string like 'a|b...|n'
     * @param $value
     *
     * @return array
     *
     * @author Chaykovskiy Roman
     */
    protected function generateArrayForParamValue($pk, $value)
    {
        $keys = explode(self::ARRAY_KEYS_SEPARATOR, $pk);
        $countOfKeys = count($keys);
        $result = [];

        $current = &$result;
        foreach ($keys as $k => $keyValue) {
            if ($countOfKeys == ($k + 1)) {
                $current[$keyValue] = $value;
            } else {
                $current[$keyValue] = [];
                // descend into the new array
                $current = &$current[$keyValue];
            }
        }

        return $result;
    }

    /**
     * Init components, Merge config.
     */
    public function init()
    {
        parent::init();

        //Merge main extension config with local extension config
        $config = include dirname(__FILE__).'/config/main.php';
        foreach ($config as $key => $value) {
            if (is_array($value)) {
                $this->{$key} = Map::mergeArray($value, $this->{$key});
            } elseif (null === $this->{$key}) {
                $this->{$key} = $value;
            }
        }

        Yii::import($this->modelMap['SystemParam']['class']);

        //Set components
        if (count($this->components)) {
            $exists = Yii::app()->getComponents(false);
            foreach ($this->components as $component => $params) {
                if (isset($exists[$component]) && is_object($exists[$component])) {
                    unset($this->components[$component]);
                } elseif (isset($exists[$component])) {
                    $this->components[$component] = \CMap::mergeArray($params, $exists[$component]);
                }
            }
            Yii::app()->setComponents(
                $this->components, false
            );
        }

        $this->_originalParams = clone Yii::app()->params;

        if (!Yii::app() instanceof CConsoleApplication) {
            //Merge controllers map
            $route = Yii::app()->urlManager->parseUrl(Yii::app()->request);
            $module = substr($route, 0, strpos($route, '/'));
            if (Yii::app()->hasModule($module) && isset($this->controllers['controllerMap'][$module])) {
                Yii::app()->getModule($module)->controllerMap = Map::mergeArray($this->controllers['controllerMap'][$module], Yii::app()->getModule($module)->controllerMap);
            } elseif (isset($this->controllers['controllerMap']['default'])) {
                Yii::app()->controllerMap = Map::mergeArray($this->controllers['controllerMap']['default'], Yii::app()->controllerMap);
            }
        }

        if (Yii::app() instanceof \CConsoleApplication) {
            //Merge commands map
            Yii::app()->commandMap = Map::mergeArray($this->commandMap, Yii::app()->commandMap);
            Yii::app()->commandMap = array_filter(Yii::app()->commandMap);
        }

        $this->prepareParams();

        Yii::setPathOfAlias('yiicod', realpath(dirname(__FILE__).'/..'));
    }
}
